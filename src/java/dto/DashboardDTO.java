/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Dell
 */
public class DashboardDTO {
    
    private int nbCompte;
    private int nbOperation;
    private int nbUtilisateur;
    private double nbMontant;
    List<OperationDTO> lops;

    public DashboardDTO() {
        
        lops= new ArrayList<>();
    }

    
    
    
    
    public int getNbCompte() {
        return nbCompte;
    }

    public void setNbCompte(int nbCompte) {
        this.nbCompte = nbCompte;
    }

    public int getNbOperation() {
        return nbOperation;
    }

    public void setNbOperation(int nbOperation) {
        this.nbOperation = nbOperation;
    }

    public int getNbUtilisateur() {
        return nbUtilisateur;
    }

    public void setNbUtilisateur(int nbUtilisateur) {
        this.nbUtilisateur = nbUtilisateur;
    }

    public double getNbMontant() {
        return nbMontant;
    }

    public void setNbMontant(double nbMontant) {
        this.nbMontant = nbMontant;
    }

    public List<OperationDTO> getLops() {
        return lops;
    }

    public void setLops(List<OperationDTO> lops) {
        this.lops = lops;
    }
    
    
}
