/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

/**
 *
 * @author Dell
 */
public class DataConnexion {
    
    private String login ;
    private String numeroComte ;
    private String password;
    private String type;

    public DataConnexion() {
        
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNumeroComte() {
        return numeroComte;
    }

    public void setNumeroComte(String numeroComte) {
        this.numeroComte = numeroComte;
    }
    
    
    
     
}
