/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import entities.Utilisateur;
import java.util.List;


/**
 *
 * @author Dell
 */
public class CompteDTO {
    private Long id;
    private String numero;
    private double montant;
    private UtilisateurDTO owner;
    
    List<OperationDTO> operations;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    public UtilisateurDTO getOwner() {
        return owner;
    }

    public void setOwner(UtilisateurDTO owner) {
        this.owner = owner;
    }

    public List<OperationDTO> getOperations() {
        return operations;
    }

    public void setOperations(List<OperationDTO> operations) {
        this.operations = operations;
    }
    
    
    
    
}
