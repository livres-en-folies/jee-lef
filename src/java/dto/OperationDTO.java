/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import entities.OperationStatut;
import entities.OperationType;
import java.util.Date;
import javax.persistence.Temporal;

/**
 *
 * @author Dell
 */
public class OperationDTO {
    private Long id;
    private double montant;
    private OperationType type;
    private OperationStatut statut;
    private Date operationDate;
    private String numeroCompte;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    public OperationType getType() {
        return type;
    }

    public void setType(OperationType type) {
        this.type = type;
    }

    public OperationStatut getStatut() {
        return statut;
    }

    public void setStatut(OperationStatut statut) {
        this.statut = statut;
    }

    public Date getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Date operationDate) {
        this.operationDate = operationDate;
    }

    public String getNumeroCompte() {
        return numeroCompte;
    }

    public void setNumeroCompte(String numeroCompte) {
        this.numeroCompte = numeroCompte;
    }
    
    
    
}
