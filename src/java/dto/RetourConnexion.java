/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import entities.Utilisateur;
import java.io.Serializable;

/**
 *
 * @author Dell
 */
public class RetourConnexion implements Serializable{
    
    private UtilisateurDTO utilisateur ;
    private String token ;
    
    
    
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UtilisateurDTO getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(UtilisateurDTO utilisateur) {
        this.utilisateur = utilisateur;
    }

   
    
    
    
}
