/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import entities.Adress;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Dell
 */
public class CreationCompteDTO {
    
    private double montant;
    private Long id;
    private String firstName;
    private String lastName; 
    private String login;
    private String password;
    private Date createdDate;
    private Adress adress;
    
}
