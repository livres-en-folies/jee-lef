/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

/**
 *
 * @author Dell
 */
public class Virement {
    
    private String numeroInit;
    private double montantInit; 
    private String numeroFinal;

    public String getNumeroInit() {
        return numeroInit;
    }

    public void setNumeroInit(String numeroInit) {
        this.numeroInit = numeroInit;
    }

    public double getMontantInit() {
        return montantInit;
    }

    public void setMontantInit(double montantInit) {
        this.montantInit = montantInit;
    }

    public String getNumeroFinal() {
        return numeroFinal;
    }

    public void setNumeroFinal(String numeroFinal) {
        this.numeroFinal = numeroFinal;
    }
    
    
    
    
}
