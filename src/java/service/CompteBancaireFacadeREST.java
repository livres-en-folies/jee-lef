/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import dto.CompteDTO;
import dto.DashboardDTO;
import dto.DepotRetraiDTO;
import dto.EndTransaction;
import dto.InitTransaction;
import dto.OperationDTO;
import dto.ResponseDTO;
import dto.RetourConnexion;
import dto.RetourInit;
import dto.RoleDTO;
import dto.UtilisateurDTO;
import dto.Virement;
import entities.CompteBancaire;
import entities.Operation;
import entities.OperationType;
import entities.Utilisateur;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import metier.CompteBancaireBean;
import metier.OperationBean;
import metier.ResponseMetier;
import metier.UtilisateurBean;
import util.AppBankException;
import util.AppBankUtil;

/**
 *
 * @author Amosse Edouard
 */
@Path("compte")
public class CompteBancaireFacadeREST {

    @EJB
    CompteBancaireBean compteBancaireBean;

    @EJB
    OperationBean operationBean;
    
    @EJB
    UtilisateurBean utilisateurBean;
    
    

    public CompteBancaireFacadeREST() {

    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public void create(CompteBancaire entity) {
        try {
            compteBancaireBean.creationCompteUtilisateur(entity);
        } catch (Exception e) {
            System.out.println("The Exception " + e.getMessage());
        }
    }

    
    
    @POST
    @Path("init_transaction")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response initTransaction(InitTransaction entity) {
          ResponseMetier<Operation> rCompteBancaire =   compteBancaireBean.initTrasactionAchatLivre(entity);
          if(rCompteBancaire!=null && rCompteBancaire.getCode()==200){
          ResponseDTO<RetourInit> responseDTO = new ResponseDTO<>();
          responseDTO.setCode(200);
          responseDTO.setMessage("Connexion Reussie" );
          RetourInit retourInit = new RetourInit();
          retourInit.setToken(AppBankUtil.issueToken(rCompteBancaire.getData().getCompteBancaire().getOwner().getLogin()));
          
          Operation opp = rCompteBancaire.getData();
          OperationDTO odto = new OperationDTO();
          odto.setId(opp.getId());
          odto.setMontant(opp.getMontant());
          odto.setNumeroCompte(opp.getCompteBancaire().getNumero());
          odto.setStatut(opp.getStatut());
          odto.setType(opp.getType());
          retourInit.setOperation(odto);
   
          responseDTO.setData(retourInit);
          return Response.status( Response.Status.OK ).entity( responseDTO ).build();
      }else{
          ResponseDTO<RetourConnexion> responseDTO = new ResponseDTO<>();
          responseDTO.setCode(400);
          responseDTO.setMessage(rCompteBancaire.getMessage() );
      	  return Response.status( Response.Status.OK ).entity( responseDTO ).build();
      }

    }
    
    
    @POST
    @Path("end_transaction")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response endTransaction(EndTransaction entity) {
          ResponseMetier<Operation> rCompteBancaire =   compteBancaireBean.endTrasactionAchatLivre(entity);
          if(rCompteBancaire!=null && rCompteBancaire.getCode()==200){
          ResponseDTO<RetourInit> responseDTO = new ResponseDTO<>();
          responseDTO.setCode(200);
          responseDTO.setMessage("Transaction Reussie" );
          RetourInit retourInit = new RetourInit();
          
          Operation opp = rCompteBancaire.getData();
          OperationDTO odto = new OperationDTO();
          odto.setId(opp.getId());
          odto.setMontant(opp.getMontant());
          odto.setNumeroCompte(opp.getCompteBancaire().getNumero());
          odto.setStatut(opp.getStatut());
          odto.setType(opp.getType());
          retourInit.setOperation(odto);
          responseDTO.setData(retourInit);
          return Response.status( Response.Status.OK ).entity( responseDTO ).build();
      }else{
          ResponseDTO<RetourConnexion> responseDTO = new ResponseDTO<>();
          responseDTO.setCode(400);
          responseDTO.setMessage(rCompteBancaire.getMessage() );
      	  return Response.status( Response.Status.OK ).entity( responseDTO ).build();
      }

    }
    
    @POST
    @Path("depot")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response depotTransaction(DepotRetraiDTO entity) {
        
          ResponseDTO<CompteBancaire> responseDTO = new ResponseDTO<>();
        try {
            CompteBancaire compteBancaire =   compteBancaireBean.depotCompte(entity.getNumero_compte(), entity.getMontant());
          
            responseDTO.setCode(200);
            responseDTO.setMessage("Operation reussie");
              return Response.status( Response.Status.OK ).entity( responseDTO ).build();
        } catch (AppBankException ex) {
             responseDTO.setCode(400);
            responseDTO.setMessage(ex.getMessage());
            return Response.status( Response.Status.NOT_FOUND ).entity( responseDTO ).build();
        }
          

    }
    
    @POST
    @Path("retrait")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response retraitTransaction(DepotRetraiDTO entity) {
        
          ResponseDTO<CompteBancaire> responseDTO = new ResponseDTO<>();
        try {
            CompteBancaire compteBancaire =   compteBancaireBean.retraitCompte(entity.getNumero_compte(), entity.getMontant());
          
            responseDTO.setCode(200);
            responseDTO.setMessage("Operation reussie");
              return Response.status( Response.Status.OK ).entity( responseDTO ).build();
        } catch (AppBankException ex) {
             responseDTO.setCode(400);
            responseDTO.setMessage(ex.getMessage());
            return Response.status( Response.Status.NOT_FOUND ).entity( responseDTO ).build();
        }
          

    }
    
    
    
    
    @PUT
    @Path("virement")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response virement(Virement entity) {
         ResponseDTO<CompteBancaire> responseDTO = new ResponseDTO<>();
        try {
            compteBancaireBean.transfertCompte(entity.getNumeroInit(), entity.getMontantInit(), entity.getNumeroFinal());
             responseDTO.setCode(200);
             responseDTO.setMessage("Operation reussie");
            // responseDTO.setData(data);
             return Response.status( Response.Status.OK ).entity( responseDTO ).build();
        } catch (AppBankException ex) {
               responseDTO.setCode(400);
             responseDTO.setMessage(ex.getMessage());
             return Response.status( Response.Status.NOT_FOUND ).entity( responseDTO ).build();
        }
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Long id) {
        compteBancaireBean.remove(compteBancaireBean.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public ResponseDTO<CompteDTO> find(@PathParam("id") Long id) {

        ResponseDTO<CompteDTO> responseDTO = new ResponseDTO();
        CompteBancaire cb = compteBancaireBean.find(id);
        if (cb == null) {
            responseDTO.setCode(400);
            responseDTO.setMessage("No data found");
        } else {
            CompteDTO cdto = new CompteDTO();
            cdto.setId(cb.getId());
            cdto.setMontant(cb.getMontant());
            cdto.setNumero(cb.getNumero());
            cdto.setOwner(new UtilisateurDTO());
            cdto.getOwner().setFirstName(cb.getOwner().getFirstName());
            cdto.getOwner().setLastName(cb.getOwner().getLastName());
            cdto.getOwner().setId(cb.getOwner().getId());
            cdto.getOwner().setLogin(cb.getOwner().getLogin());
            cdto.getOwner().setRole(new RoleDTO());

            cdto.getOwner().getRole().setDescription(cb.getOwner().getRole().getDescription());
            cdto.getOwner().getRole().setId(cb.getOwner().getRole().getId());

            List<Operation> operations = operationBean.findAllOfOneCompte(cb.getId());
            List<OperationDTO> operationDTOs = new ArrayList<>();
            for (Operation oper : operations) {
                OperationDTO odto = new OperationDTO();
                odto.setId(oper.getId());
                odto.setMontant(oper.getMontant());
                odto.setOperationDate(oper.getOperationDate());
                odto.setStatut(oper.getStatut());
                odto.setType(oper.getType());
                operationDTOs.add(odto);
            }
            cdto.setOperations(operationDTOs);
            responseDTO.setCode(200);
            responseDTO.setMessage("Data found");
            responseDTO.setData(cdto);
        }
        return responseDTO;
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public ResponseDTO findAll() {
        List<CompteBancaire> cbs = compteBancaireBean.findAll();
        List<CompteDTO> compteDTOs = new ArrayList<>();
        ResponseDTO<List<CompteDTO>> responseDTO = new ResponseDTO();

        if (cbs != null && cbs.size() > 0) {
            for (CompteBancaire cb : cbs) {
                CompteDTO cdto = new CompteDTO();

                cdto.setId(cb.getId());
                cdto.setMontant(cb.getMontant());
                cdto.setNumero(cb.getNumero());
                cdto.setOwner(new UtilisateurDTO());
                cdto.getOwner().setFirstName(cb.getOwner().getFirstName());
                cdto.getOwner().setLastName(cb.getOwner().getLastName());
                cdto.getOwner().setId(cb.getOwner().getId());
                cdto.getOwner().setLogin(cb.getOwner().getLogin());
                cdto.getOwner().setRole(new RoleDTO());

                cdto.getOwner().getRole().setDescription(cb.getOwner().getRole().getDescription());
                cdto.getOwner().getRole().setId(cb.getOwner().getRole().getId());
                compteDTOs.add(cdto);
            }
            responseDTO.setCode(200);
            responseDTO.setMessage("Operation reussie ");
            responseDTO.setData(compteDTOs);
        } else {
            responseDTO.setCode(400);
            responseDTO.setMessage("Pas de donne disponible");
        }
        return responseDTO;
    }
    
    
    @GET
    @Path("resume")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response findResume() {
         DashboardDTO dashboardDTO =  compteBancaireBean.resumeDashBoard();
         return Response.status( Response.Status.OK ).entity( dashboardDTO ).build();
    }
    
 

 

}
