/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.sun.xml.ws.client.sei.ResponseBuilder;
import dto.DataConnexion;
import dto.ResponseDTO;
import dto.RetourConnexion;
import dto.RoleDTO;
import dto.UtilisateurDTO;
import entities.CompteBancaire;
import entities.Utilisateur;
import io.jsonwebtoken.Claims;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import security.Secured;


import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.io.BufferedReader;
import java.io.StringReader;
import java.security.Key;
import java.security.spec.PKCS8EncodedKeySpec;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import javax.crypto.spec.SecretKeySpec;
import javax.ejb.EJB;
import javax.ws.rs.core.Response;
import javax.xml.bind.DatatypeConverter;
import metier.ResponseMetier;


import metier.UtilisateurBean;
import util.AppBankUtil;

/**
 *
 * @author Amosse Edouard
 */
@Path("users")
public class UtilisateurFacadeREST {
    /** TODO
     * 1/ Enlever les méthodes non nécessaires
     * 2/ Ajouter une méthode permettant d'authentifier un utilisateur par son user/password
     */
    
    @EJB
    UtilisateurBean utilisateurBean ;
    
    public UtilisateurFacadeREST() {
        //super(Utilisateur.class);
    }

   // @Secured({"ADMIN"})
    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(Utilisateur entity) {
        utilisateurBean.create(entity);
    }


    @Path("connexion")
    @POST
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response connexion(DataConnexion dataConnexion) {
      Utilisateur utilisateur =   utilisateurBean.connexion(dataConnexion.getLogin(), dataConnexion.getPassword());
      UtilisateurDTO utilisateurDTO = new UtilisateurDTO();
      if(utilisateur!=null){
          
        utilisateurDTO.setId(utilisateur.getId());
        utilisateurDTO.setFirstName(utilisateur.getFirstName());
        utilisateurDTO.setLastName(utilisateur.getLastName());
        utilisateurDTO.setId(utilisateur.getId());
        utilisateurDTO.setLogin(utilisateur.getLogin());
        
        utilisateurDTO.setRole(new RoleDTO());
        utilisateurDTO.getRole().setId(utilisateur.getRole().getId());  
        utilisateurDTO.getRole().setDescription(utilisateur.getRole().getDescription());  
          
          
          ResponseDTO<RetourConnexion> responseDTO = new ResponseDTO<>();
          responseDTO.setCode(200);
          responseDTO.setMessage("Connexion Reussie" );
          RetourConnexion retourConnexion = new RetourConnexion();
          retourConnexion.setUtilisateur(utilisateurDTO);
          retourConnexion.setToken(AppBankUtil.issueToken(utilisateur.getLogin()));
          responseDTO.setData(retourConnexion);
      	return Response.status( Response.Status.OK ).entity( responseDTO ).build();
      }else{
      	return Response.status( Response.Status.NOT_FOUND ).entity( "Utilisateur Introuvable" ).build();
      }
    }
    
    
    @Path("connexion_compte")
    @POST
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response connexionByLoginAndAccount(DataConnexion dataConnexion) {
        ResponseMetier<CompteBancaire> rCompteBancaire =   utilisateurBean.connexionByLoginAndAccount(dataConnexion.getLogin(), 
        dataConnexion.getPassword(), dataConnexion.getNumeroComte());
      if(rCompteBancaire!=null && rCompteBancaire.getCode()==200){
          ResponseDTO<RetourConnexion> responseDTO = new ResponseDTO<>();
          responseDTO.setCode(200);
          responseDTO.setMessage("Connexion Reussie" );
          RetourConnexion retourConnexion = new RetourConnexion();
          retourConnexion.setToken(AppBankUtil.issueToken(rCompteBancaire.getData().getOwner().getLogin()));
          responseDTO.setData(retourConnexion);
          return Response.status( Response.Status.OK ).entity( responseDTO ).build();
      }else{
          ResponseDTO<RetourConnexion> responseDTO = new ResponseDTO<>();
          responseDTO.setCode(400);
          responseDTO.setMessage(rCompteBancaire.getMessage() );
      	  return Response.status( Response.Status.NOT_FOUND ).entity( responseDTO ).build();
      }
    }
    
    
    @Secured
    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Long id, Utilisateur entity) {
        utilisateurBean.edit(entity);
    }

    @Secured
    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Long id) {
        utilisateurBean.remove(utilisateurBean.find(id));
    }

    @Secured({"ADMIN", "CLIENT"})
    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Utilisateur find(@PathParam("id") Long id) {
        return utilisateurBean.find(id);
    }

 //   @Secured({"ADMIN"})
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Utilisateur> findAll() {
        return utilisateurBean.findAll();
    }

    @Secured
    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Utilisateur> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return utilisateurBean.findRange(new int[]{from, to});
    }

    @Secured
    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(utilisateurBean.count());
    }

    
  
  
    
}
