/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author Dell
 */
public class AppBankException extends Exception {
    
   private int code ;
   private String message ;

   public AppBankException(int code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }
   
}
