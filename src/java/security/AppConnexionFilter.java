/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package security;

import dto.ResponseDTO;
import entities.Utilisateur;
import io.jsonwebtoken.Claims;
import java.io.IOException;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;
import javax.annotation.Priority;
import javax.ejb.EJB;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import metier.UtilisateurBean;
import util.AppBankUtil;

/**
 *
 * @author Dell
 */
@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AppConnexionFilter implements ContainerRequestFilter {

    @Context
    private ResourceInfo resourceInfo;

    @EJB
    UtilisateurBean utilisateurBean;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        final SecurityContext currentSecurityContext = requestContext.getSecurityContext();

        Claims cToken = null;

        // Get the Authorization header from the request
        String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
        System.out.println("GOD ==> "+authorizationHeader);
        // Validate the Authorization header
        if (!AppBankUtil.isTokenBasedAuthentication(authorizationHeader)) {
            requestContext.abortWith(
                    Response.status(Response.Status.UNAUTHORIZED)
                            .type(MediaType.APPLICATION_JSON)
                            .entity(new ResponseDTO<String>(401, "Pas de token 1"))
                            .header(HttpHeaders.WWW_AUTHENTICATE,
                                    AppBankUtil.AUTHENTICATION_SCHEME + " realm=\"" + AppBankUtil.REALM + "\"")
                            .build());
            return;
        }

        // Verification Validation Token
        try {
            // Validate the token
            cToken = AppBankUtil.decodeJWT(authorizationHeader);
        } catch (Exception e) {
            requestContext.abortWith(
                    Response.status(Response.Status.UNAUTHORIZED)
                            .type(MediaType.APPLICATION_JSON)
                             .entity(new ResponseDTO<String>(401, "Token invalide"))
                            .header(HttpHeaders.WWW_AUTHENTICATE,
                                    AppBankUtil.AUTHENTICATION_SCHEME + " realm=\"" + AppBankUtil.REALM + "\"")
                            .build());
        }

        if (cToken != null) {
            String username = cToken.getSubject();
            Utilisateur ut = utilisateurBean.findUtilisateurByLogin(username);
            if (ut != null) {

                // Get the resource class which matches with the requested URL
                // Extract the roles declared by it
                Class<?> resourceClass = resourceInfo.getResourceClass();
                List<String> classRoles = extractRoles(resourceClass);

                // Get the resource method which matches with the requested URL
                // Extract the roles declared by it
                Method resourceMethod = resourceInfo.getResourceMethod();
                List<String> methodRoles = extractRoles(resourceMethod);

                boolean roleAuth = false;
                if (methodRoles.isEmpty()) {
                    roleAuth = checkPermissions(classRoles, ut.getRole().getId());
                } else {
                    roleAuth = checkPermissions(methodRoles, ut.getRole().getId());
                }

                if (roleAuth == true) {

                    requestContext.setSecurityContext(new SecurityContext() {

                        @Override
                        public Principal getUserPrincipal() {
                            return () -> username;
                        }

                        @Override
                        public boolean isUserInRole(String role) {
                            return true;
                        }

                        @Override
                        public boolean isSecure() {
                            return currentSecurityContext.isSecure();
                        }

                        @Override
                        public String getAuthenticationScheme() {
                            return authorizationHeader;
                        }
                    });

                } else {
                    requestContext.abortWith(
                            Response.status(Response.Status.FORBIDDEN)
                                    .type(MediaType.APPLICATION_JSON)
                                    .entity(new ResponseDTO<String>(401, "Access to the specified resource has been forbidden "))
                                    .build());
                }

            } else {
                requestContext.abortWith(
                        Response.status(Response.Status.UNAUTHORIZED)
                                .type(MediaType.APPLICATION_JSON)
                                .entity(new ResponseDTO<String>(401, "Utilisateur Invalide"))
                                .header(HttpHeaders.WWW_AUTHENTICATE,
                                        AppBankUtil.AUTHENTICATION_SCHEME + " realm=\"" + AppBankUtil.REALM + "\"")
                                .build());
            }

        } else {
            requestContext.abortWith(
                    Response.status(Response.Status.UNAUTHORIZED)
                            .type(MediaType.APPLICATION_JSON)
                            .entity(new ResponseDTO<String>(401, "Token Invalide"))
                            .header(HttpHeaders.WWW_AUTHENTICATE,
                                    AppBankUtil.AUTHENTICATION_SCHEME + " realm=\"" + AppBankUtil.REALM + "\"")
                            .build());
        }

    }

    // Extract the roles from the annotated element
    private List<String> extractRoles(AnnotatedElement annotatedElement) {
        if (annotatedElement == null) {
            return new ArrayList<String>();
        } else {
            Secured secured = annotatedElement.getAnnotation(Secured.class);
            if (secured == null) {
                return new ArrayList<String>();
            } else {
                String[] allowedRoles = secured.value();
                return Arrays.asList(allowedRoles);
            }
        }
    }

    private boolean checkPermissions(List<String> allowedRoles, String roleUser) {
        // Check if the user contains one of the allowed roles
        if (allowedRoles == null || allowedRoles.isEmpty()) {
            return true;
        } else {
            for (String tempRole : allowedRoles) {
                if (tempRole.equalsIgnoreCase(roleUser)) {
                    return true;
                }
            }
        }

        return false;
    }

}
