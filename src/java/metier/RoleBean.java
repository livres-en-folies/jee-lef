/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

import entities.Role;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Dell
 */

@Stateless
public class RoleBean extends AbstractFacade<Role>  {

    @PersistenceContext(unitName = "WebApplication1PU")
    private EntityManager em;
    
    public RoleBean() {
        super(Role.class);
    }

    @Override
    protected EntityManager getEntityManager() {
       return em;
    }
    
}
