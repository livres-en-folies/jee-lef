/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

import entities.CompteBancaire;
import entities.Operation;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Dell
 */
@Stateless
public class OperationBean extends AbstractFacade<Operation>  {

    @PersistenceContext(unitName = "WebApplication1PU")
    private EntityManager em;
    
    public OperationBean() {
        super(Operation.class);
    }

    @Override
    protected EntityManager getEntityManager() {
         return em;    
    }
    
    
    public List<Operation> findAllOfOneCompte(Long idCompte){
        Query q = em.createQuery("select op from Operation op where op.compteBancaire.id = :idCompte");
        q.setParameter("idCompte", idCompte);
        List<Operation> c = q.getResultList(); 
        if(c!=null && c.size()>0){
            return c;
        }else{
            return new ArrayList<>();
        } 
    
    }
    
}
