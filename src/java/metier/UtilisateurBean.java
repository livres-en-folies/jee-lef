/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

import entities.CompteBancaire;
import entities.Utilisateur;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Dell
 */
@Stateless
public class UtilisateurBean  extends AbstractFacade<Utilisateur>  {
    
    @PersistenceContext(unitName = "WebApplication1PU")
    private EntityManager em;
    
    @EJB
    private CompteBancaireBean compteBancaireBean; 

    public UtilisateurBean() {
        super(Utilisateur.class);
    }
    
    public Utilisateur connexion(String login, String password){
        Query q = em.createQuery("select u from Utilisateur u where u.login = :login and u.password = :password ");
        q.setParameter("login", login);
        q.setParameter("password", password);
        List<Utilisateur> c = q.getResultList(); 
        if(c!=null && c.size()>0){
            return c.get(0);
        }else{
            return null;
        }
    }
    
    public ResponseMetier<CompteBancaire> connexionByLoginAndAccount(String login,  String password, String numeroCompte){
       ResponseMetier<CompteBancaire> responseMetier = new ResponseMetier<>();
        Query q = em.createQuery("select u from Utilisateur u where u.login = :login and u.password = :password ");
        q.setParameter("login", login);
        q.setParameter("password", password);
        List<Utilisateur> c = q.getResultList(); 
        if(c!=null && c.size()>0){
            Utilisateur utilisateur =  c.get(0);
            CompteBancaire  compteBancaire = compteBancaireBean.findOneByNumeroCompteAndIdUtilisateur(numeroCompte, utilisateur.getId());
           if(compteBancaire!=null){
                responseMetier.setCode(200);
                responseMetier.setMessage("Authentification reussie");
                responseMetier.setData(compteBancaire);
                return responseMetier ;
           }else{
                responseMetier.setCode(400);
                responseMetier.setMessage("Le numero de compte est incorrect");
                return responseMetier ;
           }
            
        }else{
            responseMetier.setCode(400);
            responseMetier.setMessage("Le User ou le Password n'existe pas ");
            return responseMetier ;
        }
    }
    
    
    
    public Utilisateur findUtilisateurByLogin(String login){
        Query q = em.createQuery("select u from Utilisateur u where u.login = :login");
        q.setParameter("login", login);
        List<Utilisateur> c = q.getResultList(); 
        if(c!=null && c.size()>0){
            return c.get(0);
        }else{
            return null;
        }
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;    
    }
}
