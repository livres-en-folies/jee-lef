/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

import dto.DashboardDTO;
import dto.EndTransaction;
import dto.InitTransaction;
import dto.OperationDTO;
import entities.Operation;
import entities.CompteBancaire;
import entities.OperationStatut;
import entities.OperationType;
import entities.Role;
import entities.Utilisateur;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import util.AppBankException;

/**
 *
 * @author Amosse Edouard
 */
@Stateless
public class CompteBancaireBean extends AbstractFacade<CompteBancaire> {

    @PersistenceContext(unitName = "WebApplication1PU")
    private EntityManager em;

    // @Resource
    // UserTransaction userTransaction ;
    @EJB
    private UtilisateurBean utilisateurBean;

    @EJB
    private RoleBean roleBean;

    @EJB
    private OperationBean operationBean;

    public CompteBancaireBean() {
        super(CompteBancaire.class);
    }

    @Override
    public void create(CompteBancaire entity) {
        super.create(entity);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void creationCompteUtilisateur(CompteBancaire entity) throws AppBankException {
        // Recuperation Role 
        Role role = roleBean.find("CLIENT");
        System.out.println("metier.CompteBancaireBean.creationCompteUtilisateur()");
        // Ouverture Transaction 
        // userTransaction.begin();
        // Verification Utilisateur 
        if (entity.getOwner().getId() == 0) {
            // Creation Utilisateur 
            entity.getOwner().setPassword(String.valueOf((int) (Math.random() * (99999 - 11111 + 1) + 11111)));
            entity.getOwner().setRole(role);
        } else {
            Utilisateur utilisateur = utilisateurBean.find(entity.getOwner().getId());
            if (utilisateur == null) {
                //em.getTransaction().rollback();
                throw new AppBankException(400, "Utilisateur Introuvable");
            } else {
                // Verifier si c'est un Admin  
                if (utilisateur.getRole().getId().equalsIgnoreCase("ADMIN")) {
                    //em.getTransaction().rollback();
                    throw new AppBankException(400, "Un Administrateur ne peut pas avoir de compte ");
                }
                entity.setOwner(utilisateur);
            }
        }

        // Creation du compte 
        entity.setNumero(genererNumeroCompte());
        Operation operation = new Operation();
        operation.setMontant(entity.getMontant());
        operation.setOperationDate(new Date());
        operation.setType(OperationType.CREATION);
        operation.setStatut(OperationStatut.COMPLETE);
        operation.setCompteBancaire(entity);
        ArrayList<Operation> lop = new ArrayList<>();
        lop.add(operation);
        entity.setOperations(lop);
        create(entity);

        // Creation de l'operation 
        //  em.persist(operation);
        // Fermeture Transaction
        //  em.getTransaction().commit();
    }

    public CompteBancaire retraitCompte(String numero_compte, double montant) throws AppBankException {
        CompteBancaire compteBancaire = findOneByNumeroCompte(numero_compte);

        if (compteBancaire != null) {

            if (montant > 0) {
                if (compteBancaire.getMontant() >= montant) {
                    //Mise a jour Compte 
                    compteBancaire.setMontant(compteBancaire.getMontant() - montant);
                    edit(compteBancaire);

                    //Enregistrement de l'operation 
                    Operation operation = new Operation();
                    operation.setCompteBancaire(compteBancaire);
                    operation.setMontant(montant);
                    operation.setOperationDate(new Date());
                    operation.setType(OperationType.RETRAIT);
                    operation.setStatut(OperationStatut.COMPLETE);
                    operationBean.create(operation);

                } else {
                    throw new AppBankException(400, "Votre solde est inferieur au montant solicité ");
                }
            } else {
                throw new AppBankException(400, "Votre Montant de retrait doit etre superieur a Zero ");
            }
        } else {
            throw new AppBankException(400, "Ce numeros de compte n'existe pas ");
        }
        return compteBancaire;
    }

    public CompteBancaire depotCompte(String numero_compte, double montant) throws AppBankException {
        CompteBancaire compteBancaire = findOneByNumeroCompte(numero_compte);
        if (compteBancaire != null) {
            if (montant > 0) {
                //Mise a jour Compte 
                compteBancaire.setMontant(compteBancaire.getMontant() + montant);
                edit(compteBancaire);

                //Enregistrement de l'operation 
                Operation operation = new Operation();
                operation.setCompteBancaire(compteBancaire);
                operation.setMontant(montant);
                operation.setOperationDate(new Date());
                operation.setType(OperationType.DEPOT);
                operation.setStatut(OperationStatut.COMPLETE);
                operationBean.create(operation);

            } else {
                throw new AppBankException(400, "Votre Montant de retrait doit etre superieur a Zero ");
            }
        } else {
            throw new AppBankException(400, "Ce numeros de compte n'existe pas ");
        }
        return compteBancaire;
    }

    public ResponseMetier<Operation> initTrasactionAchatLivre(InitTransaction initTransaction)  {
        ResponseMetier<Operation> responseMetier = new ResponseMetier<>();
        Query q = em.createQuery("select u from Utilisateur u where u.login = :login and u.password = :password ");
        q.setParameter("login", initTransaction.getLogin());
        q.setParameter("password", initTransaction.getPassword());
        List<Utilisateur> c = q.getResultList();
        if (c != null && c.size() > 0) {
            Utilisateur utilisateur = c.get(0);
            CompteBancaire compteBancaire = findOneByNumeroCompteAndIdUtilisateur(initTransaction.getNumeroCompte(), utilisateur.getId());
            if (compteBancaire != null) {
                if (initTransaction.getMontant() > 0) {
                    if (compteBancaire.getMontant() >= initTransaction.getMontant()) {
                        //Mise a jour Compte 
                        compteBancaire.setMontant(compteBancaire.getMontant() - initTransaction.getMontant());
                        edit(compteBancaire);

                        //Enregistrement de l'operation 
                        Operation operation = new Operation();
                        operation.setCompteBancaire(compteBancaire);
                        operation.setMontant(initTransaction.getMontant());
                        operation.setOperationDate(new Date());
                        operation.setType(OperationType.RETRAIT);
                        operation.setStatut(OperationStatut.PENDING);
                        operationBean.create(operation);

                        responseMetier.setCode(200);
                        responseMetier.setMessage("Transac reussie");
                        responseMetier.setData(operation);
                        return responseMetier;

                    } else {
                        responseMetier.setCode(400);
                        responseMetier.setMessage("Votre solde est inferieur au montant solicité ");
                        return responseMetier;
                    }
                } else {
                    responseMetier.setCode(400);
                    responseMetier.setMessage("Votre Montant de retrait doit etre superieur a Zero");
                    return responseMetier;
                }
            } else {
                responseMetier.setCode(400);
                responseMetier.setMessage("Le numero de compte est incorrect");
                return responseMetier;
            }

        } else {
            responseMetier.setCode(400);
            responseMetier.setMessage("Le User ou le Password n'existe pas ");
            return responseMetier;
        }

    }
    
    
     public ResponseMetier<Operation> endTrasactionAchatLivre(EndTransaction endTransaction)  {
            ResponseMetier<Operation> responseMetier = new ResponseMetier<>();
            Operation operation= operationBean.find(endTransaction.getIdOperation());
            if(operation!=null && operation.getCompteBancaire().getNumero().equalsIgnoreCase(endTransaction.getNumeroCompte())){
                if(operation.getMontant()==endTransaction.getMontant()){
                    if(operation.getStatut().equals(OperationStatut.PENDING)){
                      operation.setStatut(OperationStatut.COMPLETE);
                        operationBean.edit(operation);
                        responseMetier.setCode(200);
                        responseMetier.setMessage("Le montant n'est pas correcte");
                        responseMetier.setData(operation);
                        return responseMetier;
                    }else{
                        responseMetier.setCode(400);
                        responseMetier.setMessage("Cette trasaction est deja valider");
                        return responseMetier;
                    }
                  }else{
                    responseMetier.setCode(400);
                    responseMetier.setMessage("Le montant n'est pas correcte");
                    return responseMetier;
                }
          
            }else{
                responseMetier.setCode(400);
                responseMetier.setMessage("Le numero de compte n'est pa correcte");
                return responseMetier;
            }
              
     }

    public CompteBancaire transfertCompte(String numeroInit, double montantInit, String numeroFinal) throws AppBankException {
       CompteBancaire bancaire=null;
        try {
          retraitCompte(numeroInit, montantInit);
          bancaire=  depotCompte(numeroFinal, montantInit);
        } catch (AppBankException ex) {
           throw new  AppBankException(400, ex.getMessage());
        }
        
        return bancaire;
    }

    public CompteBancaire findOneByNumeroCompte(String numero) {
        Query q = em.createQuery("select cpt from CompteBancaire cpt where cpt.numero = :numero");
        q.setParameter("numero", numero);
        List<CompteBancaire> c = q.getResultList();
        if (c != null && c.size() > 0) {
            return c.get(0);
        } else {
            return null;
        }
    }

    public CompteBancaire findOneByNumeroCompteAndIdUtilisateur(String numero, long idUtilisateur) {
        Query q = em.createQuery("select cpt from CompteBancaire cpt where cpt.numero = :numero and cpt.owner.id = :idUtilisateur");
        q.setParameter("numero", numero);
        q.setParameter("idUtilisateur", idUtilisateur);
        List<CompteBancaire> c = q.getResultList();
        if (c != null && c.size() > 0) {
            return c.get(0);
        } else {
            return null;
        }
    }

    private String genererNumeroCompte() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("mmss");
        LocalDateTime now = LocalDateTime.now();
        //System.out.println(dtf.format(now));
        int first = (int) (Math.random() * (9999 - 1111 + 1) + 1111);
        int seconde = (int) (Math.random() * (9999 - 1111 + 1) + 1111);
        return first + "-" + seconde + "-" + dtf.format(now);
    }
    
    
    public DashboardDTO resumeDashBoard(){
        
            DashboardDTO dashboardDTO = new DashboardDTO();

            List<CompteBancaire> compteBancaires= findAll();
             List<Operation> operations = operationBean.findAll();
             List<Utilisateur> utilisateurs  = utilisateurBean.findAll();

             if(compteBancaires==null){
               dashboardDTO.setNbCompte(0);
             }else{
                 dashboardDTO.setNbCompte(compteBancaires.size());
                 double somme = 0;
                 for (CompteBancaire compteBancaire : compteBancaires) {
                     somme=somme+(compteBancaire.getMontant());
                 }
                 dashboardDTO.setNbMontant(somme);
             }
             
             if(operations==null){
               dashboardDTO.setNbOperation(0);
             }else{
                 dashboardDTO.setNbOperation(operations.size());
             }

             if(utilisateurs==null){
               dashboardDTO.setNbUtilisateur(0);
             }else{
                 dashboardDTO.setNbUtilisateur(utilisateurs.size());
             }

             List<OperationDTO> operationDTOs = new ArrayList<>();
                 for (Operation opp : operations) {
                     OperationDTO operationDTO = new OperationDTO();
                     operationDTO.setId(opp.getId());
                     operationDTO.setMontant(opp.getMontant());
                     operationDTO.setNumeroCompte(opp.getCompteBancaire().getNumero());
                     operationDTO.setOperationDate(opp.getOperationDate());
                     operationDTO.setStatut(opp.getStatut());
                     operationDTO.setType(opp.getType());
                     operationDTOs.add(operationDTO);
                 }
             dashboardDTO.setLops(operationDTOs);
            return dashboardDTO;
    }

}
