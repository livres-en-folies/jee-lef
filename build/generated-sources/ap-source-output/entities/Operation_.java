package entities;

import entities.CompteBancaire;
import entities.OperationStatut;
import entities.OperationType;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.7.v20200504-rNA", date="2021-07-04T20:16:50")
@StaticMetamodel(Operation.class)
public class Operation_ { 

    public static volatile SingularAttribute<Operation, Date> operationDate;
    public static volatile SingularAttribute<Operation, CompteBancaire> compteBancaire;
    public static volatile SingularAttribute<Operation, Double> montant;
    public static volatile SingularAttribute<Operation, Long> id;
    public static volatile SingularAttribute<Operation, OperationType> type;
    public static volatile SingularAttribute<Operation, OperationStatut> statut;

}