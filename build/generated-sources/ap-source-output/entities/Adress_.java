package entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.7.v20200504-rNA", date="2021-07-04T20:16:50")
@StaticMetamodel(Adress.class)
public class Adress_ { 

    public static volatile SingularAttribute<Adress, String> ville;
    public static volatile SingularAttribute<Adress, String> rue;
    public static volatile SingularAttribute<Adress, String> numero;
    public static volatile SingularAttribute<Adress, String> codePostal;

}