package entities;

import entities.Adress;
import entities.CompteBancaire;
import entities.Role;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.7.v20200504-rNA", date="2021-07-04T20:16:50")
@StaticMetamodel(Utilisateur.class)
public class Utilisateur_ { 

    public static volatile SingularAttribute<Utilisateur, String> firstName;
    public static volatile SingularAttribute<Utilisateur, String> lastName;
    public static volatile SingularAttribute<Utilisateur, String> password;
    public static volatile SingularAttribute<Utilisateur, Date> createdDate;
    public static volatile SingularAttribute<Utilisateur, Role> role;
    public static volatile SingularAttribute<Utilisateur, Adress> adress;
    public static volatile ListAttribute<Utilisateur, CompteBancaire> accounts;
    public static volatile SingularAttribute<Utilisateur, Long> id;
    public static volatile SingularAttribute<Utilisateur, String> login;

}