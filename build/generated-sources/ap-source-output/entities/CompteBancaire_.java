package entities;

import entities.Operation;
import entities.Utilisateur;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.7.v20200504-rNA", date="2021-07-04T20:16:50")
@StaticMetamodel(CompteBancaire.class)
public class CompteBancaire_ { 

    public static volatile SingularAttribute<CompteBancaire, Utilisateur> owner;
    public static volatile ListAttribute<CompteBancaire, Operation> operations;
    public static volatile SingularAttribute<CompteBancaire, String> numero;
    public static volatile SingularAttribute<CompteBancaire, Double> montant;
    public static volatile SingularAttribute<CompteBancaire, Long> id;

}